import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    linkActiveClass: "active-btn",
    routes: [
        {
            path: "/",
            name: "home",
            component: () => import("../views/uuid/Index.vue"),
        },
        {
            path: "/check",
            name: "check",
            component: () => import("../views/uuid/Check.vue"),
        },
        // {
        //   path: '/about',
        //   name: 'about',
        //   component: () => import('../views/AboutView.vue')
        // }
    ],
});

export default router;
